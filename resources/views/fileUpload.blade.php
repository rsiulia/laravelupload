<!DOCTYPE html>
<html>
<head>
    <title>Studiul Individual nr.4</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
      
<body>
<div class="container">
       
    <div class="panel panel-primary">
  <br><br><br>
      <div class="panel-heading">
        <h2>Studiul Individual nr.4</h2>
      </div>
  
      <div class="panel-body">
       
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
            </div>
        @endif
      
        <form action="{{ route('file.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
  
            <div class="mb-3">
                <label class="form-label" for="inputFile">Fișierul:</label>
                <input 
                    type="file" 
                    name="file" 
                    id="inputFile"
                    class="form-control @error('file') is-invalid @enderror">
  
                @error('file')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
   
            <div class="mb-3">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>
       
        </form>
<br><br>
        <h2>Fișierele încărcate:</h2>
        <br>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nume</th>
                                    <th>Extensia</th>
                                    <th>Mărimea</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $count = 1;
                                @endphp
                                @foreach ($files_info as $item)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $item['filename'] }}</td>
                                        <td>{{ $item['fileext'] }}</td>
                                        <td>
                                            {{ $item['filesize'] . ' bytes' }} /
                                            {{ round($item['filesize'] / 1000, 1) . ' KB' }} /
                                            {{ round($item['filesize'] / (1000 * 1024), 2) . ' MB' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>

      </div>
    </div>
</div>
</body>
    
</html>
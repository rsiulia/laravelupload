<?php
  
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use File;

  
class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files_info = [];

        foreach (File::allFiles(public_path('uploads')) as $file) {

            $files_info[] = array(
                "filename" => $file->getFilename(),
                "filesize" => $file->getSize(), 
                "fileext" => $file->getExtension()
            );
        }

        return view('fileUpload', [
            "files_info" => $files_info
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:pdf,png,jpeg|max:2048',
        ]);
    
        $fileName = time().'.'.$request->file->extension();  
        $request->file->move(public_path('uploads'), $fileName);
   
        /*  
            Write Code Here for
            Store $fileName name in DATABASE from HERE 
        */
     
        return back()
            ->with('success','You have successfully upload file.')
            ->with('file', $fileName);
   
    }
}
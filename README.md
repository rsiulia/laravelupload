# Studiul individual nr.4 Upload Files (Laravel 9)

![image](/uploads/d69aedf0c01bd6fb3286301f98623dce/image.png)

Încărcarea se realizează cu ajutorul metodei store() a controller-ului FileController:

Se acceptă doar fișiere cu extensia pdf,jpeg sau png.

![image](/uploads/054b5bb473feeda7369c4b96891f8919/image.png)

După încărcare se afișează conținutul folder-ului storage (fișierele încărcate) în tabel:

![image](/uploads/c55f540193bdc2146ba9aeed48918db9/image.png)

Afișarea se realizează cu ajutorul metodei index care selectează fișierele din folder și le transmite în view stocate în array-ul files-info:

![image](/uploads/fa77e4a5ebebfcb7adfd48e17b6e55af/image.png)
